package zadanie5;

public class Kwadrat {
    private int dlugoscBoku;

    public Kwadrat(int dlugoscBoku) {
        this.dlugoscBoku = dlugoscBoku;
    }

    public int obliczObwod() {
        return 4 * dlugoscBoku;
    }

    public int obliczPole() {
        return dlugoscBoku * dlugoscBoku;
    }

    public int getDlugoscBoku() {
        return dlugoscBoku;
    }

    public void setDlugoscBoku(int dlugoscBoku) {
        this.dlugoscBoku = dlugoscBoku;
    }
}
