package zadanie20;

import zadanie12.Circle;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainZad20 {

    public static void main(String[] args) {
        CircleX c = new CircleX(5);
        SquareX s = new SquareX(5);
        RectangleX r = new RectangleX(1, 2);

        List<IFigure> list = new ArrayList<>();
//        list.add(s);
        list.add(r);
        list.add(c);

        for (IFigure f : list) {
            System.out.println("Pole: " + f.obliczPole());
            System.out.println("Obwod: " + f.obliczObwod());
//            f.saySomething();
        }
    }
}
