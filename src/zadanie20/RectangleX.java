package zadanie20;

public class RectangleX implements IFigure {
    private double a, b;

    public RectangleX(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double obliczObwod() {
        return (2 * a) + (2 * b);
    }

    @Override
    public double obliczPole() {
        return a * b;
    }
}
