package zadanie20;

public class CircleX implements IFigure{
    private double r;

    public CircleX(double r) {
        this.r = r;
    }

    @Override
    public double obliczObwod() {
        return 2 * Math.PI * r;
    }

    @Override
    public double obliczPole() {
        return Math.PI * r * r;
    }
}
