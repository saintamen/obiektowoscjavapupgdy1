package zadanie20;

public interface IFigure {
    double obliczObwod();
    double obliczPole();
}
