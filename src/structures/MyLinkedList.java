package structures;

/**
 * LinkedList'a nie sortowana.
 *
 * @param <E> - typ parametrĂłw ktĂłre przechowuje lista
 */
public class MyLinkedList<E> {
    private ListElement<E> head;
    private ListElement<E> tail;

    public MyLinkedList() {
        this.head = null;
        this.tail = null;
    }

    /**
     * Dodaj element do listy.
     *
     * @param data - nowy element.
     */
    public void addElement(E data) {
        ListElement<E> listElement = new ListElement<>(data);
        if (head == null) {
            head = listElement;
            tail = listElement;
        } else {
            // lista jednokierunkowa - musze przejsc do ostatniego elementu
//            ListElement tmp = head;
//            while (tmp.getNext() != null) {
//                tmp = tmp.getNext();
//            }

            tail.setNext(listElement);
        }
    }

    /**
     * WyĹwietla elementy listy.
     */
    public void printList() {
        ListElement<E> tmp = head;
        if (head == null) return;

        System.out.println(tmp.getData());
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.println(tmp.getData());
        }
    }

    /**
     * Usun element (ostatni) z listy. Usuniecie metoda 1 (z uzyciem wskaznika/referencji na poprzedni element).
     */
    public void removeLastVersionWithPrevious() {
        ListElement<E> tmp = head;
        if (head == null) return;

        if (tmp.getNext() != null) {      // wiecej niz 1 element na liscie
            ListElement<E> tmpPrev = head; // przedostatni element

            while (tmp.getNext() != null) {
                tmpPrev = tmp;
                tmp = tmp.getNext();
            }
            tmpPrev.setNext(null);
            // tmp - to ostatni element
        } else {
            head = null;
        }
    }

    public void removeFirst() {
        if (head == tail) { // oznacza ze pierwszy i ostatni to to samo
            // wiec jest 1 element
            tail = null;
            head = null;
        } else if (head != null && tail != null) {
            head = head.getNext();
        }
    }

    /**
     * Usun element (ostatni) z listy. Usuniecie metoda 2 (z uzyciem sprawdzenia za-nastÄpnego elementu).
     */
    public void removeLast() {
        ListElement<E> tmp = head;
        if (head == null) return;

        if (tmp.getNext() != null) {
            while (tmp.getNext() != null && tmp.getNext().getNext() != null) {
                tmp = tmp.getNext();
            }

            tmp.setNext(null);
        } else if (tmp != null && tmp.getNext() == null) { // to znaczy ze jest tylko jeden
            // element head
            head = null;
        }
    }

    /**
     * Pobranie elementu o indeksie i'tym.
     *
     * @param indeks - indeks elementu
     * @return zwraca alement na itym indeksie
     * @throws ArrayIndexOutOfBoundsException - kiedy przekraczamy za tablice. (szukamy nieistniejacego elementu)
     */
    public E get(int indeks) throws ArrayIndexOutOfBoundsException {
        if (head == null) throw new ArrayIndexOutOfBoundsException(); // brak elementow - blad

        ListElement<E> tmp = head;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && counter != indeks) {
            // dopoki istnieja elementy i nie dotarlismy do konkretnego numeru elementu powtarzaj petle
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        // jesli licznik != indeks (wyszlismy z petli z powodu braku elementow [tmp.getNext() != null]
        // a nie dlatego ze dotarlismy do konkretnego elementu
        if (counter != indeks) {
            throw new ArrayIndexOutOfBoundsException(); // blad
        }

        return tmp.getData(); // zwrĂłÄ DANE ELEMENTU a nie sam element

        // przejĹcie do n-tego elementu listy i zwrĂłcenie go
        // wskazĂłwka (zrĂłb licznik przechodzenia przez pÄtlÄ while [zeby wiedziec na ktorym
        //                                                                  jestesmy elemencie)
        // wskazĂłwka (nie zapomnij sprawdziÄ czy dany element istnieje na liĹcie)
        // wskazĂłwka pro: wiesz co powinno siÄ wydarzyÄ jeĹli wykroczysz poza zakres?

//        return ?;
    }

    /**
     * Usun element na indeksie itym.
     *
     * @param indeks - indeks elementu do usuniecia.
     */
    public void removeElement(int indeks) {
        if (head == null) throw new ArrayIndexOutOfBoundsException();

        ListElement<E> tmp = head;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && ((counter + 1) != indeks)) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        if ((counter + 1) == indeks) { // tmp to element przed elementem usuwanym
            if (tmp.getNext() == null) {
                throw new ArrayIndexOutOfBoundsException();
            }
            tmp.setNext(tmp.getNext().getNext());
        }
    }

    /**
     * Oblicz rozmiar listy.
     *
     * @return - zwraca rozmiar listy.
     */
    public int size() {
        if (head == null) return 0;

        // w przeciwnym razie

        ListElement<E> tmp = head;
        int counter = 1; // licznik elementow
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }

        return counter;
    }
}