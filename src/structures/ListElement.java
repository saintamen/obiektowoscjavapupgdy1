package structures;

public class ListElement<E> {
    private E data;
    private ListElement<E> next;
    private ListElement<E> prev;

    public ListElement(E data) {
        this.data = data;
        this.next = null;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public ListElement<E> getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }
}