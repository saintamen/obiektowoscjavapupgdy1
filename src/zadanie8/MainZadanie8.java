package zadanie8;

import java.util.Scanner;

public class MainZadanie8 {
    public static void main(String[] args) {

        QuadraticEquation eq = new QuadraticEquation(2.0, 3.0, -1.0);
        System.out.println("Delta: " + eq.calculateDelta());
        System.out.println("x1: " + eq.calculateX1());
        System.out.println("x2: " + eq.calculateX2());
        /**
         * Delta: 17.0
         *  x1: 0.28077640640441515
         *  x2: -1.7807764064044151
         */

        /**
         * (bez nawiasów dla calculatex1 i calculatex2)
         * Delta: 17.0
         *  x1: 1.1231056256176606
         *  x2: -7.123105625617661
         */

        /**
         * (bez nawiasów wokół -b - Math.sqrt(delta))
         * Delta: 17.0
         *  x1: -1.9692235935955849
         *  x2: -4.030776406404415
         */


//        QuadraticEquation eq2 = new QuadraticEquation(1.0, 0.0, 0.0);
//        System.out.println("Delta: " + eq2.calculateDelta());
//        System.out.println("x1: " + eq2.calculateX1());
//        System.out.println("x2: " + eq2.calculateX2());

//        QuadraticEquation eq3 = new QuadraticEquation(-1.0, 0.0, -1.0);
//        System.out.println("Delta: " + eq3.calculateDelta());
//        System.out.println("x1: " + eq3.calculateX1());
//        System.out.println("x2: " + eq3.calculateX2());
    }
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//
//        boolean isWorking = true;
//        QuadraticEquation equation = null;
//
//        while (isWorking) {
//            String komenda = sc.next();
//            switch (komenda) {
//                case "quit":
//                    isWorking = false;
//                    break;
//                case "ustaw":
//                    double a = sc.nextDouble();
//                    double b = sc.nextDouble();
//                    double c = sc.nextDouble();
//                    equation = new QuadraticEquation(a, b, c);
//                    break;
//                case "obliczx1":
//                    System.out.println(equation.calculateX1());
//                    break;
//                case "obliczx2":
//                    System.out.println(equation.calculateX2());
//                    break;
//            }
//        }
//    }
}
