package zadanie8;

public class QuadraticEquation {
    private double a, b, c;

    // ax^2 + bx + c = 0
    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        return (b * b) - (4 * a * c);
    }

    public double calculateX1() {
        double delta = calculateDelta(); // robie jedno wyliczenie delty
        if (delta > 0) {
            return (-b + Math.sqrt(delta)) / (2 * a);
        } else if (delta < 0) {
            throw new DeltaLessThanZeroException();
        } else {
            // delta == 0
            return -b / (2 * a);
        }
    }

    public double calculateX2() {
        double delta = calculateDelta();
        if (delta > 0) {
            return (-b - Math.sqrt(delta)) / (2 * a);
        } else if (delta < 0) {
            throw new DeltaLessThanZeroException();
        } else {
            // delta == 0
//            System.out.println("Jest tylko jedno rozwiązanie");
            System.err.println("Jest tylko jedno rozwiązanie");
            throw new DeltaEqualsZeroException();
        }
    }
}
