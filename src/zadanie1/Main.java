package zadanie1;

public class Main {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("imie", 23);
        // a)
        osoba1.printYourNameAndAge();

        // b)
        System.out.println(osoba1);
        // ==
        System.out.println(osoba1.toString());

        // c)
        System.out.println(osoba1.getName() + " " + osoba1.getAge());
    }
}
