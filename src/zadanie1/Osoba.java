package zadanie1;

public class Osoba {
    private String name;
    private int age;

    public Osoba(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // pobiera/ zwraca
    public String getName() {
        return name;
    }

    // ustawia wartość
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // super bardzo mocno zle
//    public static void printYourNameAndAge() {
//        System.out.println("Dane: " + name + " " + age);
//    }
    public void printYourNameAndAge() {
        System.out.println("Dane: " + name + " " + age);
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
