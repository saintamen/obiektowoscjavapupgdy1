package zadanie4;

public class Calculator {
    public Calculator() {
    }

    public int /*lub double*/ addTwoNumbers(int a, int b) {
        return a + b;
    }

    public int /*lub double*/ subtractTwoNumbers(int a, int b) {
        return a - b;
    }

    public int /*lub double*/ multiplyTwoNumbers(int a, int b) {
        return a * b;
    }

    public int /*lub double*/ divTwoNumbers(int a, int b) {
        return a / b;
    }
}
