package zadanie4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj a:");
        int a = sc.nextInt();
        System.out.println("Podaj b:");
        int b = sc.nextInt();

        // opcja 1
        int wynik = calculator.addTwoNumbers(a, b);
        System.out.println("Wynik: " + wynik);

        // opcja 2
        System.out.println("Wynik: " + calculator.addTwoNumbers(a,b ));
    }
}
