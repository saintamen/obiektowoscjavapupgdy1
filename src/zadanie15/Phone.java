package zadanie15;

public class Phone {
    private boolean isRinging = false;
    private String phoneNumber;

    // b
    private String screenContent; // pole 'zawartosc wyswietlacza' (String)
    private String dialedNumber; // pole 'wybrany numer' (String)

    // metodę 'zadzwon' ktora zmienia wartość pola 'czy dzwoni' z false na true,
    // ale tylko wtedy kiedy jest false(nie mozna wykonac drugiego polaczenia jesli juz dzwoni)
    public void call() {
        if (!isRinging) {
            isRinging = true;
        } else {
//            System.out.println("Trwa połączenie");
            System.out.println("Trwa połączenie z numerem " + dialedNumber);
        }
    }

    // b
    // zamiast modyfikować skorzystamy z polimorfizmu metod
    // zmodyfikuj metode zadzwon tak, zeby przyjmowala parametr (numer na ktory dzwonimy)
    public void call(String numberToCall) {
        if (!isRinging) {
            isRinging = true;
            // po zmienieniu flagi 'czy dzwoni' na true ustaw 'wybrany numer' na numer z parametru.
            dialedNumber = numberToCall;

            // Ustaw pole 'zawartosc wyswietlacza' na tekst "Rozmowa z numer_telefonu".
            screenContent = "Rozmowa z " + dialedNumber;
        } else {
            System.out.println("Trwa połączenie");
        }
    }


    // metode 'rozlacz' ktora zmienia wartosc pola 'czy dzwoni' na false z true,
    // ale tylko wtedy kiedy jest true (kiedy trwa jakas rozmowa)
    public void disconnect() {
        if (isRinging) {
            isRinging = false;

            // b
            // czyścimy screen
            screenContent = "";
            // czyścimy numer do ktorego dzwonimy
            dialedNumber = null;
        } else {
            System.out.println("Nie ma trwającego połączenia");
        }
    }

    public boolean isRinging() {
        return isRinging;
    }

    public void setRinging(boolean ringing) {
        isRinging = ringing;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
