package zadanie18;

public class NoAdultException extends RuntimeException {
//public class NoAdultException extends Exception {

    public NoAdultException() {
        super("Osoba niepełnoletnia.");
    }
}
