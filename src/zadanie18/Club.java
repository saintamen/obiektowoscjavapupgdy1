package zadanie18;

public class Club {

    // Klasa Club powinna zawierać metodę enter(Person person), która
    // wyrzuca wyjątek NoAdultException, jeżeli osoba jest niepełnoletnia.
    public void enter(Person osoba) throws NoAdultException {
        if (osoba.getAge() < 18) {
            throw new NoAdultException();
        }
        System.out.println(osoba.getName() + " wchodzi do klubu");
    }
}
