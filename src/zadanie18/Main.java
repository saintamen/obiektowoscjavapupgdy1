package zadanie18;

public class Main {
    public static void main(String[] args) {
        Person p = new Person("yo", 17);

        Club klub = new Club();

        try {
            klub.enter(p);
        } catch (NoAdultException exc) {
            System.err.println(p.getName() + " został wywalony." + exc.getMessage());
        }
    }
}
