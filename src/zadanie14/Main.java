package zadanie14;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Parcel p = new Parcel();
        Scanner sc = new Scanner(System.in);
        boolean isWorking = true;
        while (isWorking) {
            String slowo = sc.next();
            if (slowo.equals("s")) { // send
                System.out.println("Komenda wyslania.");
                p.send();
            } else if (slowo.equals("sp")) { // send polecony
                System.out.println("Wysłanie poleconego.");
                p.sendRegistered();
            } else if (slowo.equals("r")) { // ustaw receiver
                System.out.println("Podaj receivera:");
                slowo = sc.next();

                p.setReceiver(slowo);
                System.out.println("Receiver ustawiony.");
            } else if (slowo.equals("n")) { // stworz nowa paczke
                p = new Parcel();
                System.out.println("Nowa paczka: " + p);
            } else if (slowo.equals("sn")) { // ustaw sendera
                System.out.println("Podaj nadawce:");
                slowo = sc.next();

                p.setSender(slowo);
                System.out.println("Sender ustawiony.");
            } else if (slowo.equals("z")) { // ustaw zawartosc
                p.setHasContent(true);
                System.out.println("Zawartość ustawiona.");
            } else if (slowo.equals("spr")) {
                System.out.println(p);
            }
        }
    }
}
