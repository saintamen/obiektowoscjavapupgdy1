package zadanie14;

public class Parcel {
    private String receiver; // odbiorca
    private String sender; // nadawca
    private boolean isSent; // czy zostala wyslana
    private boolean hasContent; // czy ma zawartosc

    //dodaj domyślny (pusty) konstruktor
    public Parcel() {
    }

    //oraz konstruktor z odbiorcą i flagą zawartość
    public Parcel(String receiver, boolean hasContent) {
        this.receiver = receiver;
        this.hasContent = hasContent;
    }

    public void send() {
        // metodę 'wyślij' która sprawdza czy jest zawartość,
        // i czy jest odbiorca i wyświetla komunikat o nadaniu jeśli jest zawartość i odbiorca
        // receiver == null = brak odbiorcy
        if (hasContent && receiver != null) {
            System.out.println("Wysylam paczke.");
            // oraz ustawia flagę 'czy została wysłana' na true.
            if (isSent) {
                // nie ma wtreści - jesli wyslana to nie wysylaj.
                System.out.println("Paczka byla juz wyslana");
            } else {
                isSent = true;
                System.out.println("Paczka wyslana");
            }
        } else {
            System.out.println("Nie ma zawartosci lub odbiorcy.");
        }
    }

    public void sendRegistered() {
        //która sprawdza to samo co wyżej, ale sprawdza również czy
        if (hasContent && receiver != null && sender != null) {
            System.out.println("Wysylam paczke polecona.");
            // oraz ustawia flagę 'czy została wysłana' na true.
            if (isSent) {
                // nie ma wtreści - jesli wyslana to nie wysylaj.
                System.out.println("Paczka byla juz wyslana");
            } else {
                isSent = true;
                System.out.println("Paczka wyslana");
            }
        } else {
            System.out.println("Nie ma zawartosci/odbiorcy/nadawcy.");
        }
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    public boolean isHasContent() {
        return hasContent;
    }

    public void setHasContent(boolean hasContent) {
        this.hasContent = hasContent;
    }

    @Override
    public String toString() {
        return "Parcel{" +
                "receiver='" + receiver + '\'' +
                ", sender='" + sender + '\'' +
                ", isSent=" + isSent +
                ", hasContent=" + hasContent +
                '}';
    }
}
