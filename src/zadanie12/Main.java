package zadanie12;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String komenda = sc.nextLine();
            //  [0]   [1] [2]
            // Obwod kolo 5
            // pole kolo 5
            // obwod Prostokat 5 3
            // pole prOstokat 3 2
            // pole kwadrat 5
            // quit

            String komendSameMaleLitery = komenda.toLowerCase().trim();
            // jesli komenda rozpoczyna sie od "quit"
            if (komendSameMaleLitery.startsWith("quit")) {
                // konczymy prace programu
                isWorking = false;
                break;
            }

            // podzieli nam komende na slowa
            String[] slowa = komendSameMaleLitery.split(" ");

            try {
                String ksztalt = slowa[1];
                // sprawdzamy czy wprowadzony ksztalt to kolo
                if (ksztalt.equals("kolo")) {
                    // trzecim slowem komendy  (np. obwod kolo 5) jest promien
                    String dlugoscPromienia = slowa[2];
                    // parsowanie - zamiana string na int
                    int promien = Integer.parseInt(dlugoscPromienia);

                    Circle kolo = new Circle(promien);

                    if (slowa[0].equals("obwod")) {
                        System.out.println(kolo.obliczObwod());
                    } else if (slowa[0].equals("pole")) {
                        System.out.println(kolo.obliczPole());
                    }
                    // sprawdzammy czy ksztalt to prostokat
                } else if (ksztalt.equals("prostokat")) {
                    // trzecim i czwartym slowem komendy  (np. obwod prostokat 5 3) jest dlugosc boku a i b
                    String dlugoscA = slowa[2];
                    String dlugoscB = slowa[3];
                    // obwod prostokat 5 - wyskoczy wyjatek ArrayIndexOutOfBoundsException
                    // obwod prostokat 5 a - wyskoczy wyjatek NumberFormatException
                    // parsowanie - zamiana string na int
                    int dlA = Integer.parseInt(dlugoscA);
                    int dlB = Integer.parseInt(dlugoscB);

                    Rectangle prostokat = new Rectangle(dlA, dlB);

                    if (slowa[0].equals("obwod")) {
                        System.out.println(prostokat.obliczObwod());
                    } else if (slowa[0].equals("pole")) {
                        System.out.println(prostokat.obliczPole());
                    }
                } else if (ksztalt.equals("kwadrat")) {
                    String dlugoscBoku = slowa[2];
                    // parsowanie - zamiana string na int
                    int bok = Integer.parseInt(dlugoscBoku);

                    // tworzenie kwadratu
                    Square kwadrat = new Square(bok);

                    switch (slowa[0]) {
                        case "obwod":
                            System.out.println(kwadrat.obliczObwod());
                            break;
                        case "pole":
                            System.out.println(kwadrat.obliczPole());
                            break;
                    }
//                    if (slowa[0].equals("obwod")) {
//                        System.out.println(kwadrat.obliczObwod());
//                    } else if (slowa[0].equals("pole")) {
//                        System.out.println(kwadrat.obliczPole());
//                    }
                }
            } catch (NumberFormatException nfe) {
                // jesli wprowadzimy znaki nie liczbowe i bedziemy je parsowac
                System.err.println("Niepoprawna wartość parametru");
            } catch (ArrayIndexOutOfBoundsException aioobe) {
                // jesli wykroczymy poza indeks tablicy - za malo parametrow
                System.err.println("Niepoprawna ilość parametrów");
            }
        }
    }
}
