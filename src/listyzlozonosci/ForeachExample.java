package listyzlozonosci;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ForeachExample {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(19);
        lista.add(18);
        lista.add(17);
        lista.add(16);
        lista.add(15);
        lista.add(14);
        lista.add(13);
        lista.add(12);
        lista.add(11);
        // i        :  0  1  2  3  4  5  6  7  8
        // wartosci : 19 18 17 16 15 14 13 12 11
        for (int i = 0; i < lista.size(); i++) {
            Integer wartosc = lista.get(i);
            System.out.println("Indeks; " + i + " element: " + wartosc);
        }
//
//        // wartosci : 19 18 17 16 15 14 13 12 11
        long czasStart = System.nanoTime();
        for (Integer wartosc : lista) {
            System.out.println("Element: " + wartosc);
//            int indeks = lista.indexOf(wartosc);
        }
//        System.out.println(System.nanoTime() - czasStart);
        long czasStop = System.nanoTime();
        System.out.println(czasStop - czasStart);
//        lista.add(2);
//        lista.add(0, 2);
        List<Boolean> listsss = new ArrayList<>(Arrays.asList(false, false, false, true));

        for (int i = 0; i < listsss.size(); i++) {
            Boolean wartosc = listsss.get(i);
            listsss.remove(i);
            listsss.add(i, !wartosc);
        }
        System.out.println(listsss);
    }

}
