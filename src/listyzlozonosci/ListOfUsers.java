package listyzlozonosci;

import java.util.ArrayList;

public class ListOfUsers {
    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList<>();

        list.add(new User("a", 1));
        list.add(new User("b", 2));
        list.add(new User("c", 3));
        list.add(new User("d", 4));
        list.add(new User("e", 5));
        list.add(new User("f", 6));

        System.out.println(list);

        User tenDrugi = list.get(1);
        tenDrugi.setName("abrakadabra");

        System.out.println(list);
    }
}
