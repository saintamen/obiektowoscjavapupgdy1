package typwyliczeniowy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Card karta = new Card(2, CardType.DIAMONDS);

        Scanner sc = new Scanner(System.in);
        boolean isWorking = true;
        while (isWorking) {
            String slowo = sc.next();

            Gender type = null;

            // MAN ! OK
            // WOMAN ! OK
            // awdawdawdawawd- IllegalArgumentException
            try {
                type = Gender.valueOf(slowo.toUpperCase());
            } catch (IllegalArgumentException iae) {
                System.out.println("zly parametr");
                type = Gender.OTHER;
            }

//            if (slowo.equals("MAN")) {
//                System.out.println();
//                type = Gender.MAN;
//            } else if (slowo.equals("WOMAN")) {
//                System.out.println();
//                type = Gender.WOMAN;
//            }

            System.out.println(type);
        }
    }
}
