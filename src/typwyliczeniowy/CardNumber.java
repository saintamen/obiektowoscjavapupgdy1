package typwyliczeniowy;

public enum CardNumber {
    C2(2), C3(3), C4(4);
    private int value;

    CardNumber(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
