package zadanie7;

public class MainZadanie7 {
    public static void main(String[] args) {
        // obiektowo
        MyMath math = new MyMath();

        // odwołania w kontekście obiektowym
        // musi byc obiekt na ktorym ja moge wywolac metode
        System.out.println(math.abs(1));
        System.out.println(math.abs(1.0));

        // statycznie
        // wywolania z kontekstu statycznego
        // nie potrzebujemy obiektu
        // piszemy: NAZWA_KLASY.NAZWA_METODY(PARAMETRY);
        System.out.println(MyStaticMath.abs(1));
        System.out.println(MyStaticMath.abs(1.0));
    }
}
