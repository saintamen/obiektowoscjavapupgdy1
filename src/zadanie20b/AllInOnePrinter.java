package zadanie20b;

public class AllInOnePrinter implements IPrinter, ICopier, IFaxMachine {
    @Override
    public String copy(String something) {
        return "Copy of: " + something;
    }

    @Override
    public void faxSomething(String something) {
        System.out.println("Faxing");
    }

    @Override
    public void print() {
        System.out.println("Printing");
    }
}
