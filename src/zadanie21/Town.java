package zadanie21;

import java.util.ArrayList;
import java.util.List;

public class Town {
    private List<Citizen> list = new ArrayList<>();

    public Town() {
    }

    public void addCitizen(Citizen character) {
        // dodaje postać do miasta
        // dodaje obywatela miasta
        list.add(character);
    }

    public int howManyCanVote() {
        int counter = 0;
        for (Citizen citizen : list) {
            if (citizen.canVote()) { // jesli obywatel moze glosowac
                counter++;
            }
        }
        System.out.println(counter + " osob moze glosowac");
        return counter;
    }

    public void printAllCitizens() {
        for (Citizen citizen : list) {
            // każdy obywatel wypisze kim jest i czy moze glosowac:
//            System.out.println(citizen);
            if (citizen instanceof King) {
                System.out.println("King " + citizen.canVote());
            } else if (citizen instanceof Townsman) {
                System.out.println("Townsman " + citizen.canVote());
            } else if (citizen instanceof Soldier) {
                System.out.println("Soldier " + citizen.canVote());
            } else if (citizen instanceof Peasant) {
                System.out.println("Peasant " + citizen.canVote());
            }
        }
    }
}
