package zadanie16;

public class Main {
    public static void main(String[] args) {
        Computer pc = new Computer();
        pc.setCpuPower(7.0);
//        pc.setGPUName("atari");
        pc.setDiskSpace(10);
        pc.setRam(8);
        System.out.println(pc.isGamingPC());
    }
}
