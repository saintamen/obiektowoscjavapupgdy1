package zadanie6;

public class BankAccount {
    private double iloscPieniedzyNaKoncie;
    private int bankAccountNumber;

    public BankAccount(double iloscPieniedzyNaKoncie) {
        this.iloscPieniedzyNaKoncie = iloscPieniedzyNaKoncie;
    }

    /**
     * Stwórz program a w nim klasę "BankAccount". Konto powinno przechowywać ilość pieniedzy na koncie.
     * konto bankowe powinno mieć metodę dodawania pieniędzy do konta, oraz odejmowania :
     * (metoda addMoney która w parametrze przyjmuje ilosc pieniedzy
     * do dodania oraz substractMoney która jako parametrz przyjmuje ilosc pieniedzy do odjecia).
     * Dodaj do klasy metodę "printBankAccountStatus" która powinna wypisywać stan konta.
     * <p>
     * UWAGA! nie twórz pól klasy do metod odejmowania i dodawania pieniedzy na koncie.
     * To powinny być parametry metody!
     */
    // dodaje pieniadze na konto (! nie zwracam)
    public void addMoney(double iloscPieniedzyDoDodania) {
        iloscPieniedzyNaKoncie += iloscPieniedzyDoDodania;
    }

    public void subtractMoney(double iloscPieniedzyDoWyplaty) {
        iloscPieniedzyNaKoncie -= iloscPieniedzyDoWyplaty;
    }

    public void printBankAccountStatus() {
        System.out.println("Stan konta: " + iloscPieniedzyNaKoncie);
    }

    public double getIloscPieniedzyNaKoncie() {
        return iloscPieniedzyNaKoncie;
    }
}
