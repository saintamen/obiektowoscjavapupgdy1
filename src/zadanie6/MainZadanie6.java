package zadanie6;

public class MainZadanie6 {
    public static void main(String[] args) {
        BankAccount kontoBankowe1 = new BankAccount(0);

        kontoBankowe1.printBankAccountStatus(); // 0
        kontoBankowe1.addMoney(100);
        kontoBankowe1.printBankAccountStatus(); // 100
        kontoBankowe1.addMoney(1000); // 1100
        kontoBankowe1.subtractMoney(200); // 900
        kontoBankowe1.printBankAccountStatus(); // 900

        BankAccount kontoBankowe2 = new BankAccount(0);
        kontoBankowe2.addMoney(200);
        kontoBankowe2.addMoney(400);

        kontoBankowe2.subtractMoney(1000);

        kontoBankowe1.printBankAccountStatus(); // 900
        kontoBankowe2.printBankAccountStatus(); // -400


    }
}
