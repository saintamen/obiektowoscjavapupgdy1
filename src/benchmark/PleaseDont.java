package benchmark;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PleaseDont {
    public static void main(String[] args) {
        linked();
    }

    public static void linked() {
        List<Integer> a = new LinkedList<>();
        // dodanie inicjalnej ilosci obiektow
        for (int i = 0; i < 1000000; i++) {
            a.add(i);
        }

        // czas wykonania petli foreach. W tym przypadku
        // zlozonosc takiej petli to n
        long nanoStart = System.currentTimeMillis();
        for (Integer v : a) {
            int tmp = v;
        }
        long nanoStop = System.currentTimeMillis();
        System.out.println("Czas Linked foreach: " + (nanoStop - nanoStart));

        // czas wykonania petli for. W tym przypadku
        // z kazdym obiegiem musimy ocenic rozmiar listy (size - zlozonosc n)
        // oraz dotrzec do konkretnego elementu ze zlozonoscia n.
        // zlozonosc takiej petli to n*n
        nanoStart = System.currentTimeMillis();
        for (int i = 0; i < a.size(); i++) {
            int tmp = a.get(i);
        }
        nanoStop = System.currentTimeMillis();
        System.out.println("Czas Linked for: " + (nanoStop - nanoStart));
    }

    public static void array() {
        List<Integer> a = new ArrayList<>();
        // dodanie inicjalnej ilosci obiektow
        for (int i = 0; i < 1000000; i++) {
            a.add(i);
        }

        // czas wykonania petli foreach. W tym przypadku
        // zlozonosc takiej petli to n
        long nanoStart = System.currentTimeMillis();
        for (Integer v : a) {
            int tmp = v;
        }
        long nanoStop = System.currentTimeMillis();
        System.out.println("Czas Array foreach: " + (nanoStop - nanoStart));

        // czas wykonania petli for. W tym przypadku
        // size jest jasno znany wiec otrzymanie rozmiaru to czas staly,
        // wyciagniecie elementu z arraylist to czas staly
        // zlozonosc petli to n
        nanoStart = System.currentTimeMillis();
        for (int i = 0; i < a.size(); i++) {
            int tmp = a.get(i);
        }
        nanoStop = System.currentTimeMillis();
        System.out.println("Czas Array for: " + (nanoStop - nanoStart));
    }
}
